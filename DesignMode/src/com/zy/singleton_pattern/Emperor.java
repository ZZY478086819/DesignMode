package com.zy.singleton_pattern;

public class Emperor {
	private static Emperor emperor = null;

	// ˽�л�������
	private Emperor() {

	}

	public  static Emperor getInstance() {
		if (emperor == null) {
			synchronized (Emperor.class) {
				if (emperor == null) {
					emperor = new Emperor();
				}
			}
		}
		return emperor;
	}

	// �ʵ۽�ʲô����ѽ
	public void emperorInfo() {
		System.out.println("�Ҿ��ǻʵ�ĳĳĳ....");
	}
}
