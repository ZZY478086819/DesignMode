package com.zy.state_pattern;

/**
 * 
 * @author zy 模拟电梯的动作
 */
public class Client {
	public static void main(String[] args) {
		ILift lift = new Lift();
//		// 首先是电梯门开启，人进去
//		lift.open();
//
//		// 然后电梯门关闭
//		lift.close();
//
//		// 再然后，电梯跑起来，向上或者向下
//		lift.run();
//
//		// 最后到达目的地，电梯挺下来
////		lift.stop();
//		// 电梯的初始条件应该是停止状态
//		lift.setState(ILift.STOPPING_STATE);
//		// 首先是电梯门开启，人进去
//		lift.open();
//		// 然后电梯门关闭
//		lift.close();
//		// 再然后，电梯跑起来，向上或者向下
//		lift.run();
//		// 最后到达目的地，电梯挺下来
//		lift.stop();
		//Client 调用类太简单了，只要定义个电梯的初始状态，然后调用相关的方法，就完成了，完全不用考虑状态的变更
		Context context = new Context();
		context.setLiftState(new ClosingState());
		context.open();
		context.close();
		context.run();
		context.stop();
	}
}
