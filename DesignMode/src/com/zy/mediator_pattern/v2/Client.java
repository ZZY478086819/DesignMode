package com.zy.mediator_pattern.v2;

/**
 * 
 * @author zy 场景类是怎么运行
 *   在场景类中增加了一个中介者，然后分别传递到三个同事类中，三个类都具有相同的特性
 */
public class Client {
	public static void main(String[] args) {
		AbstractMediator mediator = new Mediator(); 
		System.out.println("------采购人员采购电脑--------");
		Purchase purchase = new Purchase(mediator);
		purchase.buyIBMcomputer(100);

		// 销售人员销售电脑
		System.out.println("\n------销售人员销售电脑--------");
		Sale sale = new Sale(mediator); 
		sale.sellIBMComputer(1);

		// 库房管理人员管理库存
		System.out.println("\n------库房管理人员清库处理--------");
		Stock stock = new Stock(mediator); 
		stock.clearStock();
	}
}
