package com.zy.mediator_pattern.v2;

/**
 * 
 * @author zy
 *	 中介者 Mediator 有定义了多个 Private 方法，其目标是处理各个对象之间的依赖关系，即是说把原有
一个对象要依赖多个对象的情况移到中介者的 Private 方法中实现.
 */
public abstract class AbstractMediator {
	protected Purchase purchase;
	protected Sale sale;
	protected Stock stock;

	// 构造函数
	public AbstractMediator() {
		purchase = new Purchase(this);
		sale = new Sale(this);
		stock = new Stock(this);
	}

	// 中介者最重要的方法，叫做事件方法，处理多个对象之间的关系
	public abstract void execute(String str, Object... objects);

}
