package com.zy.mediator_pattern.general;

/**
 * 
 * @author zy 同事的抽象类
 */
public class Colleague {
	protected Mediator mediator;

	public Colleague(Mediator _mediator) {
		this.mediator = _mediator;
	}
}
