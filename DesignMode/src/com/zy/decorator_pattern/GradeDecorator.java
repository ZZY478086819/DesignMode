package com.zy.decorator_pattern;

public class GradeDecorator extends Decorator {

	public GradeDecorator(SchoolReport sr) {
		super(sr);
	}

	// 我要汇报年级成绩（呦呵，居然是前100）
	private void reportHighScore() {
		System.out.println("这次考试年级排70名");
	}

	@Override
	public void report() {
		this.reportHighScore();
		super.report();
	}
}
