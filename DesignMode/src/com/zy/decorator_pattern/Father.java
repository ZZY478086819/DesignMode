package com.zy.decorator_pattern;

/**
 * 
 * @author zy 老爸看成绩了
 * 可以看到不同的Decorator，层层包装，每个类中都会实现一次自己的装饰，
 * 既不用多层继承，又充分说明了多态的好处
 */
public class Father {
	public static void main(String[] args) {
		//成绩单拿过来
		 SchoolReport sr; 
		 sr = new FouthGradeSchoolReport(); //原装的成绩单
		 
		//加了最高分说明的成绩单
		 sr = new HighScoreDecorator(sr); 
		
		//又加了成绩排名的说明
		 sr = new SortDecorator(sr);
		 
		 sr=new GradeDecorator(sr);
		//看成绩单
		sr.report();
		
		//然后老爸，一看，很开心，就签名了
		sr.sign("老七"); 
	}
}
