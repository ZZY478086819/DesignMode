package com.zy.abstract_factory_pattern;

/**
 * 
 * 
 * @author zy
 * 女娲建立起了两条生产线，分别是：
 * 男性生产线
 * 女性生产线
 *
 */
public class NvWa {
	public static void main(String[] args) {
		// 第一条生产线，男性生产线
		HumanFactory maleHumanFactory = new MaleHumanFactory();
		// 第二条生产线，女性生产线
		HumanFactory femaleHumanFactory = new FemaleHumanFactory();
		// 生产线建立完毕，开始生产人了:
		//生成黄种男人
		Human maleYellowHuman = maleHumanFactory.createYellowHuman();
		maleYellowHuman.cry();
		maleYellowHuman.laugh();
		maleYellowHuman.talk();
		maleYellowHuman.sex();
		
		//生成黄种女人
		Human feMaleYellowHuman = femaleHumanFactory.createYellowHuman();
		feMaleYellowHuman.cry();
		feMaleYellowHuman.laugh();
		feMaleYellowHuman.talk();
		feMaleYellowHuman.sex();
	}
}
