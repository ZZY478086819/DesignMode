package com.zy.abstract_factory_pattern;
/**
 * 
 * @author zy
 * 女性黑种人
 */
public class BlackFemaleHuman extends BlackHuman {

	@Override
	public void sex() {
		System.out.println("该黑种人的性别为女...");
	}
}
