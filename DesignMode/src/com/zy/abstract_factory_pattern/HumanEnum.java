package com.zy.abstract_factory_pattern;


/*
 * 世界上有哪些类型的人，列出来
 */
public enum HumanEnum {
	YellowMaleHuman("com.zy.abstract_factory_pattern.YellowMaleHuman"),
	YellowFemaleHuman("com.zy.abstract_factory_pattern.YellowFemaleHuman"),
	WhiteFemaleHuman("com.zy.abstract_factory_pattern.WhiteFemaleHuman"),
	WhiteMaleHuman("com.zy.abstract_factory_pattern.WhiteMaleHuman"),
	BlackFemaleHuman("com.zy.abstract_factory_pattern.BlackFemaleHuman"),
	BlackMaleHuman("com.zy.abstract_factory_pattern.BlackMaleHuman");
	private String value = "";
	//定义构造函数，目的是Data(value)类型的相匹配
	private HumanEnum(String value){
		 this.value = value;
	}
	public String getValue(){
		 return this.value;
	} 
}
