package com.zy.abstract_factory_pattern;
/**
 * 
 * @author zy
 * 女性白种人
 */
public class WhiteFemaleHuman extends WhiteHuman {

	@Override
	public void sex() {
		System.out.println("该白种人的性别为女....");
	}

}
