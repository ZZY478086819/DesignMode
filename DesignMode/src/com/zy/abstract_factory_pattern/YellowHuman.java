package com.zy.abstract_factory_pattern;


/**
 * 
 * @author zy
 * 黄种人抽象
 */
public abstract class YellowHuman implements Human {

	
	@Override
	public void laugh() {
		System.out.println("黄种人会大笑，幸福呀！"); 

	}

	@Override
	public void cry() {
		System.out.println("黄种人会哭"); 

	}

	@Override
	public void talk() {
		System.out.println("黄种人会说话，一般说的都是双字节"); 

	}
}
