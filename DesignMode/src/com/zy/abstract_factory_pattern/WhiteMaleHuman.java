package com.zy.abstract_factory_pattern;
/**
 * 
 * @author zy
 * 男性白种人
 */
public class WhiteMaleHuman  extends WhiteHuman{

	@Override
	public void sex() {
		System.out.println("该白种人的性别为男...."); 
		
	}
}
