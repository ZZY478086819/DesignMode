package com.zy.abstract_factory_pattern;
/**
 * 
 * @author zy
 * 男性黑种人
 */
public class BlackMaleHuman  extends BlackHuman{

	@Override
	public void sex() {
		System.out.println("该黑种人的性别为男...");
	}
	
}	
