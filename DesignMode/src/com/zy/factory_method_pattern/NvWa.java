package com.zy.factory_method_pattern;

public class NvWa {
	public static void main(String[] args) {
		for(int i=0;i<10;i++) {
			System.out.println("\\n\\n------------随机产生人种了-----------------"+i);
			Human human=HumanFactory.createHuman();
			human.cry();
			human.laugh();
			human.talk();
		}
	}
}
