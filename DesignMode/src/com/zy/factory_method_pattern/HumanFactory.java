package com.zy.factory_method_pattern;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * 
 * @author zy
 *	女娲，神仙呀，造人出来呀，然后捏泥巴，放八卦炉
 *  我们把这个生产人的过程用Java程序表现出来：
 */
public class HumanFactory {
	//定义一个MAP,初始化过的Human对象都放在这里
	private static HashMap<String,Human> humans = new HashMap<String,Human>(); 
	//定一个烤箱，泥巴塞进去，人就出来，这个太先进了
	public static Human createHuman(Class c) {
		Human human=null;
		try {
			//如果MAP中有，则直接从取出，不用初始化了(延迟加载)
			if(humans.containsKey(c.getSimpleName())) {
				human=humans.get(c.getSimpleName());
			}else {
				human = (Human)Class.forName(c.getName()).newInstance(); 
				humans.put(c.getSimpleName(), human);
			}
		}catch(InstantiationException e){ //你要是不说个人种颜色的话，没法烤，要白的黑，你说话了才好烤
			System.out.println("必须指定人种的颜色"); 
		}catch(IllegalAccessException e) { // //定义的人种有问题，那就烤不出来了，这是...
			System.out.println("人种定义错误！"); 
		}catch(ClassNotFoundException e) { //你随便说个人种，我到哪里给你制造去？！
			System.out.println("你指定的人种找不到！"); 
		}
		return human;
	}
	//随机造人
	public static Human createHuman() {
		Human human=null;
		//首先是获得有多少个实现类，多少个人种
		List<Class> concreteHumanList = ClassUtils.getAllClassByIntterface(Human.class);
		Random random=new Random();
		int rand=random.nextInt(concreteHumanList.size());
		human=createHuman(concreteHumanList.get(rand));
		return human;
	}
}
