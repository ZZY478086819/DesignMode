package com.zy.flyweight_pattern.v4;

/**
 * 
 * @author zy 场景类
 */
public class Client {
	public static void main(String[] args) {
		// 在对象池中初始化4个对象
		SignInfoFactory.getSignInfo("科目1");
		SignInfoFactory.getSignInfo("科目2");
		SignInfoFactory.getSignInfo("科目3");
		SignInfoFactory.getSignInfo("科目4");

		// 模拟每个线程都到对象池中获得对象，造成了变量共享的问题，所以这里的外部状态尽量为全局唯一
		SignInfo signInfo = SignInfoFactory.getSignInfo("科目2");
		//这里用死循环来放大线程的安全问题
		while (true) {
			signInfo.setId("ZhangSan");
			signInfo.setLocation("ZhangSan");
			new MultiThread(signInfo).start();
			signInfo.setId("LiSi");
			signInfo.setLocation("LiSi");
			new MultiThread(signInfo).start();
		}
	}
}
