package com.zy.flyweight_pattern.v4;

/**
 * 
 * @author zy 为了展示多线程的情况，我们写一个多线程的类
 */
public class MultiThread extends Thread {
	private SignInfo signInfo;

	public MultiThread(SignInfo _signInfo) {
		this.signInfo = _signInfo;
	}

	@Override
	public void run() {
		if (!signInfo.getId().equals(signInfo.getLocation())) {
			System.out.println("编号：" + signInfo.getId());
			System.out.println("考试地址：" + signInfo.getLocation());
			System.out.println("线程不安全了！");
		}
	}
}
