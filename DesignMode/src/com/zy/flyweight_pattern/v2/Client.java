package com.zy.flyweight_pattern.v2;

/**
 * 
 * @author zy 场景类
 */
public class Client {
	public static void main(String[] args) {
		// 从工厂中获得一个对象
		// 初始化对象池
		for (int i = 0; i < 4; i++) {
			String subject = "科目" + i;
			// 初始化地址
			for (int j = 0; j < 4; j++) {
				String key = subject + "考试地点" + j;
				SignInfoFactory.getSignInfo(key.trim());
			}
		}
		SignInfo signInfo = SignInfoFactory.getSignInfo("科目1考试地点1".trim());

		
		
//		// 初始化对象池
//		SignInfoFactory.getSignInfo("科目1");
//		// 计算执行100万次需要的时间
//		long currentTime = System.currentTimeMillis();
//		for (int i = 0; i < 1000000; i++) {
//			SignInfoFactory.getSignInfo("科目1");
//		}
//		long tailTime = System.currentTimeMillis();
//		System.out.println("执行时间：" + (tailTime - currentTime) + " ms");

	}
}
