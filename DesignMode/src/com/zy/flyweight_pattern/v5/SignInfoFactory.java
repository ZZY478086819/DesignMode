package com.zy.flyweight_pattern.v5;

import java.util.HashMap;

/**
 * 
 * @author zy 报考信息工厂
 */
public class SignInfoFactory {
	// 池容器
	private static HashMap<ExtrinsicState, SignInfo> pool = new HashMap<ExtrinsicState, SignInfo>();
	
	// 池容器
		private static HashMap<String, SignInfo> pool1 = new HashMap<String, SignInfo>();
		
	// 报名信息的对象工厂
	@Deprecated
	public static SignInfo getSignInfo() {
		return new SignInfo();
	}

	// 从池中获得对象
	public static SignInfo getSignInfo(ExtrinsicState key) {
		// 设置返回对象
		SignInfo result = null;
		//// 池中没有该对象，则建立，并放入池中
		if (!pool.containsKey(key)) {
			result = new SignInfo4Pool(key);
			pool.put(key, result);
		} else {
			result = pool.get(key);
		}
		return result;
	}
	// 从池中获得对象
		public static SignInfo getSignInfo(String key) {
			// 设置返回对象
			SignInfo result = null;
			//// 池中没有该对象，则建立，并放入池中
			if (!pool1.containsKey(key)) {
				result = new SignInfo();
				pool1.put(key, result);
			} else {
				result = pool.get(key);
			}
			return result;
		}
}
