package com.zy.flyweight_pattern.v5;

/**
 * 
 * @author zy 外部状态类（作为map的key 必须覆写equals 和 HashCode方法）
 */
public class ExtrinsicState {
	// 考试科目
	private String subject;
	// 考试地点
	private String location;

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	@Override
	public int hashCode() {
		return subject.hashCode() + location.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof ExtrinsicState) {
			ExtrinsicState state = (ExtrinsicState)obj;
			return state.getLocation().equals(this.location)&&
					state.getSubject().equals(this.subject);
		}
		return false;
	}
}
