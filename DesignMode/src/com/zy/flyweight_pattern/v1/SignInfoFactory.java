package com.zy.flyweight_pattern.v1;

/**
 * 
 * @author zy 报考信息工厂
 */
public class SignInfoFactory {
	// 报名信息的对象工厂
	public static SignInfo getSignInfo() {
		return new SignInfo();
	}
}
