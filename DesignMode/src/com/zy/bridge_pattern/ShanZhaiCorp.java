package com.zy.bridge_pattern;

/**
 * 
 * @author zy
 *	我是山寨老大，你流行啥我就生产啥
 *	HouseCorp 类和 ShanZhaiCorp 类的区别是在有参构造的参数类型上，HouseCorp 类比较明确，我就是
 *	只要 House 类，所以直接定义传递进来的必须是 House 类， 一个类尽可能少的承担职责，那方法也是一样，
 *	既然 HouseCorp 类已经非常明确只生产 House 产品，那为什么不定义成 House 类型呢？ShanZhaiCorp 就
 *	不同了，它是确定不了生产什么类型。	
 */
public class ShanZhaiCorp extends Corp {
	// 产什么产品，不知道，等被调用的才知道
	public ShanZhaiCorp(Product product) {
		super(product);
	}

	// 狂赚钱
	public void makeMoney() {
		super.makeMoney();
		System.out.println("我赚钱呀...");
	}
}
