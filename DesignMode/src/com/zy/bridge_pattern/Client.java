package com.zy.bridge_pattern;

public class Client {
	public static void main(String[] args) {
		System.out.println("房地产公司成立");
		HouseCorp hc=new HouseCorp(new House());
		hc.makeMoney();
		System.out.println("服装公司成立");
		ShanZhaiCorp sz1=new ShanZhaiCorp(new Clothes());
		sz1.makeMoney();
		System.out.println("iPod公司成立");
		ShanZhaiCorp sz2=new ShanZhaiCorp(new IPod());
		sz2.makeMoney();
	}
}
