package com.zy.template_method_pattern;

/**
 * 
 * @author zy Hummer Model是悍马车辆模型 这里强调一下为什么大部分的方法都是protected：因为一旦模板定义好之后是不会修改的，
 *         就好比造钱的模板定义出来之后，印了几百亿的美金，然后要改模板，这不是胡闹吗
 *         run方法设置为final是因为，从车停止到启动上路，再到停止，这个过程其中的顺序在车制造出来之后就是固定的，不需要修改
 */
public abstract class HummerModel {
	
	// 发动（手摇发动/电力发动）
	protected abstract void start();

	// 刹车
	protected abstract void stop();

	// 喇叭会出声音，是滴滴叫，还是哔哔叫
	protected abstract void alarm();

	// 引擎会轰隆隆的响，不响那是假的
	protected abstract void engineBoom();

	// 那模型应该会跑吧，别管是人退的，还是电力驱动，总之要会跑
	final public void run() {
		// 先发动汽车
		this.start();

		// 引擎开始轰鸣
		this.engineBoom();

		// 喇嘛想让它响就响，不想让它响就不响
		if (this.isAlarm()) {
			this.alarm();
		}
		// 到达目的地就停车
		this.stop();
	}

	// 钩子方法，默认喇叭是会响的
	protected abstract boolean isAlarm();
}
