package com.zy.template_method_pattern;

/**
 * 
 * @author zy 悍马H1 ，虽然模板是一定的，但是引擎，喇叭，发动机，刹车制动 这些都是任意配置的
 */
public class HummerH1Model extends HummerModel {
	private boolean alarmFlag = true; // 是否要响喇叭

	@Override
	protected void alarm() {
		System.out.println("悍马H1鸣笛...");
	}

	@Override
	protected void engineBoom() {
		System.out.println("悍马H1引擎声音是这样在...");
	}

	@Override
	protected void start() {
		System.out.println("悍马H1发动...");
	}

	@Override
	protected void stop() {
		System.out.println("悍马H1停车...");
	}

	// 外部设置是否可以按喇叭
	public void setAlarmFlag(boolean alarmFlag) {
		this.alarmFlag = alarmFlag;
	}

	@Override
	protected boolean isAlarm() {
		return this.alarmFlag;
	}
}
