package com.zy.template_method_pattern;

/**
 * 
 * @author zy 悍马H1
 */
public class HummerH2Model extends HummerModel {
	private boolean alarmFlag = true; // 是否要响喇叭
	@Override
	protected void alarm() {
		System.out.println("悍马H2鸣笛...");
	}

	@Override
	protected void engineBoom() {
		System.out.println("悍马H2引擎声音是这样在...");
	}

	@Override
	protected void start() {
		System.out.println("悍马H2发动...");
	}

	@Override
	protected void stop() {
		System.out.println("悍马H2停车...");
	}

	// 外部设置是否可以按喇叭
	public void setAlarmFlag(boolean alarmFlag) {
		this.alarmFlag = alarmFlag;
	}

	@Override
	protected boolean isAlarm() {
		return this.alarmFlag;
	}
}
