package com.zy.template_method_pattern;

/**
 * 
 * @author zy
 *	客户就比较轻松了，只要给够了钱，车随你挑，车模随你看
 */
public class Client {
	public static void main(String[] args) {
		// 客户开着H1型号，出去遛弯了
		HummerH1Model h1 = new HummerH1Model();

		h1.run(); // 汽车跑起来了；

		// 客户开H2型号，出去玩耍了
		HummerH2Model h2 = new HummerH2Model();
		h2.run();
	}
}
