package com.zy.proxy_pattern;

public interface Worker {
	//找公司
	public void FindCompany();
	//谈合作
	public void WorkCollaborate();
	//拍宣传
	public void TakePublicity();
	//唱歌
	public void Sing();
	// 收钱
	public void CollectMoney();
}
