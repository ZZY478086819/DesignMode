package com.zy.proxy_pattern;

/**
 * 
 * 
 * @author zy
 * 定一个潘金莲是什么样的人
 */
public class PanJinLian implements KindWomen {

	@Override
	public void makeEyesWithMan() {
		System.out.println("潘金莲抛媚眼"); 
	}

	@Override
	public void makeLoverWithMan() {
		System.out.println("潘金莲在和男人做那个....."); 
	}
}
