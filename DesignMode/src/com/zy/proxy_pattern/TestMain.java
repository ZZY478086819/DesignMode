package com.zy.proxy_pattern;

public class TestMain {
	public static void main(String[] args) {
		Singer singer=new Singer();
		Broker broker=new Broker(singer);
		broker.FindCompany();
		broker.WorkCollaborate();
		broker.Sing();
		broker.TakePublicity();
		broker.CollectMoney();
	}
}
