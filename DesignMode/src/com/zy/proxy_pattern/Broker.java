package com.zy.proxy_pattern;

public class Broker implements Worker {
	private 	Singer singer;
	public Broker(Singer singer) {
		this.singer=singer;
	}
	@Override
	public void FindCompany() {
		System.out.println("经济人找公司");
	}

	@Override
	public void WorkCollaborate() {
		System.out.println("经济人谈合作");
	}

	@Override
	public void TakePublicity() {
		singer.TakePublicity();
	}

	@Override
	public void Sing() {
		singer.Sing();
	}
	@Override
	public void CollectMoney() {
		System.out.println("经济人收钱");
	}
}
