package com.zy.proxy_pattern;

/**
 * 
 * @author zy
 * 定义一个西门庆，这人色中饿鬼
 */
public class XiMenQing {
	public static void main(String[] args) {
		/*
		 * 水浒里是这样写的：西门庆被潘金莲用竹竿敲了一下难道，痴迷了，
		 * 被王婆看到了, 就开始撮合两人好事，王婆作为潘金莲的代理人
		 * 收了不少好处费，那我们假设一下：
		 * 如果没有王婆在中间牵线，这两个不要脸的能成吗？难说的很！
		 */
		//把王婆叫出来
		System.out.println("西门庆找了王婆");
		WangPo wangPo = new WangPo();
		
		//然后西门庆就说，我要和潘金莲happy，然后王婆就安排了西门庆丢筷子的那出戏:
		wangPo.makeEyesWithMan(); 
		wangPo.makeLoverWithMan(); 
	}
}
