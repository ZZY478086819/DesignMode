package com.zy.builder_pattern;

public class Client {
	public static void main(String[] args) {
		Director director = new Director();
		//100辆A类型的奔驰车模型
		for (int i = 0; i < 100; i++) {
			director.getABenzModel().run();
		}
		//100辆D类型的宝马车
		for (int i = 0; i < 100; i++) {
			director.getDBMWModel().run();
		}
	}
}
