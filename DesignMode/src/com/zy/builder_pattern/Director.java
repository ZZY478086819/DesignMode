package com.zy.builder_pattern;

import java.util.ArrayList;

/**
 * 
 * @author zy 增加了 Director 类，这个类是做了层封装，类中的方法说明如下： A
 *         型号的奔驰车辆模型是只有启动（start）、停止(stop)方法，其他的引擎声音、喇叭都没有; B 型号的奔驰车是先发动引擎（engine
 *         boom）,然后启动(star),再然后停车(stop),没有喇叭； C
 *         型号的宝马车是先喇叭叫一下（alarm），然后（start）,再然后是停车(stop)，引擎不轰鸣； D
 *         型号的宝马车就一个启动(start)，然后一路跑到黑，永动机，没有停止方法，没有喇叭，没有引擎轰鸣； E 型号、F
 *         型号…等等，可以有很多，启动(start)、停止(stop)、喇叭(alarm)、引擎轰鸣(engine boom)
 *         安排好不同的顺序，生产车辆模型
 */
public class Director {
	private ArrayList<String> sequence = new ArrayList();
	private BenzBuilder benzBuilder = new BenzBuilder();
	private BMWBuilder bmwBuilder = new BMWBuilder();

	/*
	 * A类型的奔驰车模型，先start,然后stop,其他什么引擎了，喇叭一概没有
	 */
	public BenzModel getABenzModel() {
		// 清理场景，这里是一些初级程序员不注意的地方
		this.sequence.clear();

		// 这只ABenzModel的执行顺序
		this.sequence.add("start");
		this.sequence.add("stop");

		// 按照顺序返回一个奔驰车
		this.benzBuilder.setSequence(this.sequence);
		return (BenzModel) this.benzBuilder.getCarModel();

	}

	/*
	 * B型号的奔驰车模型，是先发动引擎，然后启动，然后停止，没有喇叭
	 */
	public BenzModel getBBenzModel() {
		this.sequence.clear();

		this.sequence.add("engine boom");
		this.sequence.add("start");
		this.sequence.add("stop");

		this.benzBuilder.setSequence(this.sequence);
		return (BenzModel) this.benzBuilder.getCarModel();
	}

	/*
	 * C型号的宝马车是先按下喇叭（炫耀嘛），然后启动，然后停止
	 */
	public BMWModel getCBMWModel() {
		this.sequence.clear();

		this.sequence.add("alarm");
		this.sequence.add("start");
		this.sequence.add("stop");

		this.bmwBuilder.setSequence(this.sequence);
		return (BMWModel) this.bmwBuilder.getCarModel();
	}

	/*
	 * D类型的宝马车只有一个功能，就是跑，启动起来就跑，永远不停止，牛叉
	 */
	public BMWModel getDBMWModel() {
		this.sequence.clear();

		this.sequence.add("start");

		this.bmwBuilder.setSequence(this.sequence);
		return (BMWModel) this.bmwBuilder.getCarModel();
	}

}
