package com.zy.command_pattern;

/**
 * 
 * @author zy 项目组分成了三个组，每个组还是要接受增删改的命令
 * 大家看抽象类中的每个方法，是不是每个都是一个命令？找到它，增加，删除，给我计划！是不是命令，这也就是命令模式中的命令接收者角色(Receiver)
 */
public abstract class Group {
	// 甲乙双方分开办公，你要和那个组讨论，你首先要找到这个组
	public abstract void find();

	// 被要求增加功能
	public abstract void add();

	// 被要求删除功能
	public abstract void delete();

	// 被要求修改功能
	public abstract void change();

	// 被要求给出所有的变更计划
	public abstract void plan();
}
