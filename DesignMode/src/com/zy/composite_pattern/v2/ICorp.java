package com.zy.composite_pattern.v2;

/**
 * 
 * @author zy 公司类，定义每个员工都有信息
 *
 */
public interface ICorp {
	// 每个员工都有信息，你想隐藏，门儿都没有！
	public String getInfo();
}
