package com.zy.composite_pattern.v2;

import java.util.ArrayList;

/**
 * 
 * @author zy 
 *	树枝节点，也就是各个部门经理和组长的角色
 */
public interface IBranch {

	// 增加叶子节点
	public void add(ICorp leaf);

	// 获得下级信息
	public ArrayList getSubordinateInfo();
}
