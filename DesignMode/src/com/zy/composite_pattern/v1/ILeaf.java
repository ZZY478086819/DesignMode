package com.zy.composite_pattern.v1;

/**
 * 
 * @author zy
 * 	叶子节点，也就是最小的小兵了，只能自己干活，不能指派别人了
 *
 */
public interface ILeaf {
	//获得自己的信息呀
	public String getInfo(); 
}
