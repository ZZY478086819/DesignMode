package com.zy.composite_pattern.v3;

import java.util.ArrayList;

/**
 * 
 * @author zy 所有的树枝节点
 *
 */
public class Branch extends Corp {
	// 存储子节点的信息
	private ArrayList<Corp> subordinateList = new ArrayList<Corp>();
	// 树枝节点的名称
	private String name = "";
	// 树枝节点的职位
	private String position = "";
	// 树枝节点的薪水
	private int salary = 0;

	// 通过构造函数传递树枝节点的参数
	public Branch(String _name, String _position, int _salary) {
		super(_name, _position, _salary);
	}

	// 增加一个下属，可能是小头目，也可能是个小兵
	public void add(Corp leaf) {
		leaf.setParent(this);  //设置父节点
		this.subordinateList.add(leaf);
	}

	// 我有哪些下属
	public ArrayList<Corp> getSubordinateInfo() {
		return this.subordinateList;
	}

}
