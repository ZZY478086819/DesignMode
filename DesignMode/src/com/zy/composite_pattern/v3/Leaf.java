package com.zy.composite_pattern.v3;

/**
 * 
 * @author zy 最小的叶子节点
 */
public class Leaf extends Corp {

	// 通过构造函数传递信息
	public Leaf(String _name, String _position, int _salary) {
		super(_name, _position, _salary);
	}
}
