package com.zy.visitor_pattern.v4;

public interface ITotalVisitor extends IVisitor {
	// 统计所有员工工资总和
	public void totalSalary();
}
