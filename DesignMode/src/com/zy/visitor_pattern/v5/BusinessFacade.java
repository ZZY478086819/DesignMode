package com.zy.visitor_pattern.v5;

/**
 * 
 * @author zy
 *	业务组件接口
 */
public interface BusinessFacade {
	 public void doSomething();
}
