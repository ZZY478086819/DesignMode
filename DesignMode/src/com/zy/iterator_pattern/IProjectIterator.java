package com.zy.iterator_pattern;

import java.util.Iterator;

/**
 * 
 * @author zy
 *	定义个Iterator接口
 *一般都自己先写一个接口继承，然后再继承自己写的接口，保证自己的实现类只用实现自己写的接口
 */
public interface IProjectIterator extends Iterator{
	
}	
