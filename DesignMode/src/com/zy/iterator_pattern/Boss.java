package com.zy.iterator_pattern;

/**
 * 
 * @author zy 老板来看项目信息了
 */
public class Boss {
	public static void main(String[] args) {
		// 定义一个List，存放所有的项目对象
		IProject project = new Project();
		// 增加星球大战项目
		project.add("星球大战项目ddddd", 10, 100000);
		// 增加扭转时空项目
		project.add("扭转时空项目", 100, 10000000);
		// 增加超人改造项目
		project.add("超人改造项目", 10000, 1000000000);
		IProjectIterator iterator = project.iterator();
		while(iterator.hasNext()) {
			Project next = (Project)iterator.next();
			System.out.println(next);
		}
	}
}
