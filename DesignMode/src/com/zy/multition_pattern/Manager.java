package com.zy.multition_pattern;

import java.util.ArrayList;
import java.util.Random;

public class Manager {
	//最多只能有两个经理
	private static int maxNumOfManager=2;
	//装Manager的列表
	private static ArrayList<Manager> managerList=new ArrayList(maxNumOfManager);
	//装Manager信息的列表
	private static ArrayList<String> managerInfoList=new ArrayList(maxNumOfManager);
	private static int countNumOfManager=0;  //正在工作的时哪一个经理
	
	static {
		for(int i =0;i<maxNumOfManager;i++) {
			managerList.add(new Manager("经理_"+i));
		}
	}
	private Manager() {
		
	}
	private Manager(String info) {
		managerInfoList.add(info);
	}
	//获取经理实例
	public static Manager getInstance() {
		Random r=new Random();
		//随机找出一个经理干活
		countNumOfManager=r.nextInt(maxNumOfManager);
		return managerList.get(countNumOfManager);
	}
	//获取正在工作的经理的信息
	public static void managerInfo() {
		System.out.println(managerInfoList.get(countNumOfManager));
	}
}
