package com.zy.observer_pattern;

import java.util.ArrayList;
import java.util.Observable;

/**
 * 
 * @author zy 韩非子，李斯的师弟，韩国的重要人物
 */
public class HanFeiZi extends Observable {
	// 韩非子要吃饭了
	public void haveBreakfast() {
		System.out.println("韩非子:开始吃饭了...");
		// 通知所有的观察者
		super.setChanged();
		super.notifyObservers("韩非子在吃饭");
	}

	// 韩非子开始娱乐了,古代人没啥娱乐，你能想到的就那么多
	public void haveFun() {
		System.out.println("韩非子:开始娱乐了...");
		// 通知所有的观察者
		super.setChanged();
		super.notifyObservers("韩非子在娱乐");
	}

//	/**
//	 * 
//	 * 下面都是get set方法： 通过 isHaveBreakfast和 isHaveFun 这两个布尔型变量来判断韩非子是否在吃饭或者娱乐
//	 */
//	public boolean isHaveBreakfast() {
//		return isHaveBreakfast;
//	}
//
//	public void setHaveBreakfast(boolean isHaveBreakfast) {
//		this.isHaveBreakfast = isHaveBreakfast;
//	}
//
//	public boolean isHaveFun() {
//		return isHaveFun;
//	}
//
//	public void setHaveFun(boolean isHaveFun) {
//		this.isHaveFun = isHaveFun;
//	}

}
