package com.zy.observer_pattern;

import java.util.Observable;
import java.util.Observer;

/**
 * 
 * @author zy 李斯这个人，是个观察者，只要韩非子一有动静，这边就知道
 */
public class LiSi implements Observer {

	// 汇报给秦始皇
	protected void reportToQiShiHuang(String reportContext) {
		System.out.println("李斯：报告，秦老板！韩非子有活动了--->" + reportContext);
	}

	@Override
	public void update(Observable o, Object arg) {
		System.out.println("李斯:观察到韩非子活动，开始向老板汇报了...");
		this.reportToQiShiHuang(arg.toString());
		System.out.println("李斯：汇报完毕，秦老板赏给他两个萝卜吃吃...\n");
	}
}
