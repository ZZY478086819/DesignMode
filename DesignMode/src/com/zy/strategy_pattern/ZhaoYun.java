package com.zy.strategy_pattern;

/**
 * 然后就是赵云雄赳赳的揣着三个锦囊，拉着已步入老年行列的、还想着娶纯情少女的、色迷迷的刘老
 * 爷子去入赘了，嗨，还别说，小亮的三个妙计还真是不错，瞅瞅： 
 * @author zy
 *
 */
public class ZhaoYun {
	/**
	 * 赵云出场了，他根据诸葛亮给他的交代，依次拆开妙计
	 */
	public static void main(String[] args) {
		Context context; 
		//刚刚到吴国的时候拆第一个
		System.out.println("-----------刚刚到吴国的时候拆第一个-------------");
		context = new Context(new BackDoor()); //拿到妙计
		context.operate();  //执行
		System.out.println("\n\n");
		
		//刘备乐不思蜀了，拆第二个了
		System.out.println("-----------刘备乐不思蜀了，拆第二个了-------------");
		context = new Context(new GivenGreenLight()); //拿到妙计
		context.operate();  //执行
		System.out.println("\n\n");
		
		
		//孙权的小兵追了，咋办？拆第三个
		System.out.println("-----------孙权的小兵追了，咋办？拆第三个-------------");
		context = new Context(new BlockEnemy()); //拿到妙计
		context.operate();  //执行
		System.out.println("\n\n");
	}
}
