package com.zy.strategy_pattern;


/**
 * 
 * 锦囊,计谋有了，那还要有锦囊
 * @author zy
 *
 */
public class Context {
	//构造函数，你要使用哪个妙计
	private  IStrategy straegy;
	public Context(IStrategy straegy) {
		this.straegy=straegy;
	}
	//使用计谋了，看我出招了
	public void operate() {
		this.straegy.operate();
	}
}
