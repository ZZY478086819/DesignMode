package com.zy.facade_pattern;

/**
 * 
 * @author zy 由于上述的步骤，只要client出错一步，信就无法寄出， 为了解决这个问题，我们这里使用过门面模式，增加了一个门面，Client
 *         调用时，直接调用门面的方法就可以了，不用了解具体的实现方法 以及相关的业务顺序。
 *         用户此时只需要调用ModenPostOffice.sendLetter方法，传入相应的参数，就可以将写信到寄信这一复杂的步骤完成
 *
 */
public class ModenPostOffice {
	private LetterProcess letterProcess = new LetterProcessImpl();

	// 写信，封装，投递，一体化了
	public void sendLetter(String context, String address) {

		// 帮你写信
		letterProcess.writeContext(context);

		// 写好信封
		letterProcess.fillEnvelope(address);

		// 把信放到信封中
		letterProcess.letterIntoEnvelope();

		// 邮递信件
		letterProcess.sendLetter();
	}
}
