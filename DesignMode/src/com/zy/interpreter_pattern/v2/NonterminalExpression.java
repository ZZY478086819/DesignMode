package com.zy.interpreter_pattern.v2;

import javax.naming.Context;

/**
 * 
 * @author Administrator
 *	每个非终结符表达式都代表了一个文法规则，并且每个文法规则都只关心自己周边的文
法规则的结果（注意是结果），因此这就产生了每个非终结符表达式调用自己周边的非终结
符表达式，然后最终、最小的文法规则就是终结符表达式.
 */
public class NonterminalExpression extends Expression {
	// 每个非终结符表达式都会对其他表达式产生依赖
	public NonterminalExpression(Expression... expression) {
	}

	public Object interpreter(Context ctx) {
		// 进行文法处理
		return null;
	}
}
