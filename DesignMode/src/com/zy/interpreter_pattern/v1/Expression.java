package com.zy.interpreter_pattern.v1;

import java.util.HashMap;

/**
 * 
 * @author zy
 *	抽象表达式类
 */
public abstract class Expression {
	//解析公式和数值，其中var中的key值是公式中的参数，value值是具体的数字
	public abstract int interpreter(HashMap<String,Integer> var);
}
