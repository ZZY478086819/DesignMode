package com.zy.interpreter_pattern.v1;

import java.util.HashMap;

/**
 * 
 * @author zy
 *	变量解析器
 */
public class VarExpression extends Expression {
	private String key;

	public VarExpression(String _key) {
		this.key = _key;
	}
	//从map中取之
	@Override
	public int interpreter(HashMap<String, Integer> var) {
		return var.get(key);
	}
}
