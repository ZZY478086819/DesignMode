package com.zy.interpreter_pattern.v1;

/**
 * 
 * @author zy 抽象运算符号解析器
 * 每个运算符号都只和自己左右两个数字有关系，但左
	右两个数字有可能也是一个解析的结果，无论何种类型，都是Expression的实现类，于是在
	对运算符解析的子类中增加了一个构造函数，传递左右两个表达式。
 */
public abstract class SymbolExpression extends Expression {
	protected Expression left;
	protected Expression right;

	// 所有的解析公式都应只关心自己左右两个表达式的结果
	public SymbolExpression(Expression _left, Expression _right) {
		this.left = _left;
		this.right = _right;
	}
}
