package com.zy.adapter_pattern;

import java.util.Map;

/**
 * 
 * @author zy
 * 把OuterUser包装成UserInfo
 */
public class OuterUserInfo extends OuterUser implements IUserInfo {
	private Map baseInfo = super.getUserBaseInfo(); // 员工的基本信息
	private Map homeInfo = super.getUserHomeInfo(); // 员工的家庭 信息
	private Map officeInfo = super.getUserOfficeInfo(); // 工作信息

	// 姓名
	@Override
	public String getUserName() {
		System.out.println("姓名叫做...");
		return null;
	}

	// 获得家庭地址
	@Override
	public String getHomeAddress() {
		System.out.println("这里是员工的家庭地址....");
		return null;
	}

	// 手机号码
	@Override
	public String getMobileNumber() {
		System.out.println("这个人的手机号码是0000....");
		return null;
	}

	// 办公室电话
	@Override
	public String getOfficeTelNumber() {
		System.out.println("办公室电话是....");
		return null;
	}

	// 员工的职位，是部门经理还是小兵
	@Override
	public String getJobPosition() {
		System.out.println("这个人的职位是BOSS....");
		return null;
	}

	// 获得家庭电话号码
	@Override
	public String getHomeTelNumber() {
		System.out.println("员工的家庭电话是....");
		return null;
	}

}
