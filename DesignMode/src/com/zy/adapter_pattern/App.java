package com.zy.adapter_pattern;

public class App {
	public static void main(String[] args) {
		//之前调用
		//IUserInfo userInfo=new UserInfo();
		//使用适配器
		IUserInfo userInfo=new OuterUserInfo();
		for(int i=0;i<101;i++) {
			userInfo.getMobileNumber();
		}
		/**
		 * 上面只是new的对象不同，使用了多态，其他原先的代码不需要修改，这也是适配器模式的好处
		 */
	}
}
