package com.zy.adapter_pattern;

import java.util.Map;

/**
 * 
 * @author zy 外系统的人员信息
 *
 */
public interface IOuterUser {
	// 基本信息，比如名称，性别，手机号码了等
	public Map getUserBaseInfo();

	// 工作区域信息
	public Map getUserOfficeInfo();

	// 用户的家庭信息
	public Map getUserHomeInfo();
}
