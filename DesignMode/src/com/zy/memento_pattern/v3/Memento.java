package com.zy.memento_pattern.v3;

/**
 * 
 * @author zy 这是一个忘备录类，用于记录boy初始状态
 *
 */
public class Memento {

	// 男孩的状态
	private String state = "";

	public Memento(String _state) {
		this.state = _state;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
}
