package com.zy.memento_pattern.v3;

/**
 * 
 * @author zy 建立一个管理类，就是管理这个备忘录
 */
public class Caretaker {
	// 备忘录对象
	private Memento memento;

	public Memento getMemento() {
		return memento;
	}

	public void setMemento(Memento memento) {
		this.memento = memento;
	}
}
