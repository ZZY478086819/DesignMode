package com.zy.memento_pattern.many_status;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

/**
 * 
 * @author zy BeanUtils工具类
 */
public class BeanUtils {
	// 把bean的所有属性及数值放入到Hashmap中
	public static HashMap<String, Object> backupProp(Object bean) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		try {
			// 获得Bean描述
			BeanInfo beanInfo = Introspector.getBeanInfo(bean.getClass());
			// 获得属性描述
			PropertyDescriptor[] descriptors = beanInfo.getPropertyDescriptors();

			for (PropertyDescriptor descriptor : descriptors) {
				// 属性名称
				String name = descriptor.getName();
				// 读取属性的方法
				Method readMethod = descriptor.getReadMethod();
				// 读取属性值
				Object value = readMethod.invoke(bean, new Object[] {});
				if (!name.equalsIgnoreCase("class")) {
					result.put(name, value);
				}
			}
		} catch (IntrospectionException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			e.printStackTrace();
		}
		return result;
	}

	// 把HashMap的值返回到bean中
	public static void restoreProp(Object bean, HashMap<String, Object> propMap) {
		{
			// 获得Bean描述
			BeanInfo beanInfo;
			try {
				beanInfo = Introspector.getBeanInfo(bean.getClass());
				// 获得属性描述
				PropertyDescriptor[] descriptors = beanInfo.getPropertyDescriptors();
				// 遍历所有属性
				for (PropertyDescriptor des : descriptors) {
					//属性名称
					String fieldName = des.getName();
					//如果有这个属性
					if(propMap.containsKey(fieldName)) {
						Method writeMethod = des.getWriteMethod();
						writeMethod.invoke(bean, new Object[]{propMap.get(fieldName)});
					}
				}
			} catch (IntrospectionException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			}
		}
	}
}
