package com.zy.memento_pattern.clone_memento;

public class Client {
	public static void main(String[] args) throws CloneNotSupportedException {
		//定义发起人
		Originator originator = new Originator();
		//建立初始状态
		originator.setState("初始状态...");
		System.out.println("初始状态是："+originator.getState());
		//建立备份
		originator.createMemento();
		
		//修改状态
		originator.setState("修改后的状态...");
		System.out.println("修改后状态是："+originator.getState());
		
		originator.restoreMemento();
		System.out.println("恢复后状态是："+originator.getState());
	}
}
