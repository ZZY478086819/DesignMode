package com.zy.memento_pattern.many_memento;

/**
 * 
 * @author zy
 * 	看场景类如何调用
 *
 */
public class Client {
	public static void main(String[] args) {
		//定义出发起人
		Originator originator = new Originator();
		//初始状态
		originator.setState("init statue");
		System.out.println("初始状态："+originator.getState());
		//定义出备忘录管理员
		Caretaker caretaker = new Caretaker();
		//录入忘备录01
		IMemento createMemento_1 = originator.createMemento();
		caretaker.setMemento("001", createMemento_1);
		System.out.println("状态001录入");
		//录入忘备录02
		originator.setState("first change");
		IMemento createMemento_2 = originator.createMemento();
		caretaker.setMemento("002", createMemento_2);
		System.out.println("状态002录入");
		
		//录入忘备录03
		originator.setState("second change");
		IMemento createMemento_3 = originator.createMemento();
		caretaker.setMemento("003", createMemento_3);
		System.out.println("状态003录入");
		
		
		//恢复到002
		originator.restoreMemento(caretaker.getMemento("002"));
		System.out.println("当前状态为："+originator.getState());
	
	}
}
