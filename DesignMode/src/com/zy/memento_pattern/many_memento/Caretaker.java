package com.zy.memento_pattern.many_memento;

import java.util.HashMap;

/**
 * 
 * @author zy 备忘录管理员
 * 注意：内存溢出问题，该备份一旦产生就装入内存，没有任何销毁的意向，这是非常危
	险的。因此，在系统设计时，要严格限定备忘录的创建，建议增加Map的上限，否则系统很
	容易产生内存溢出情况。
 */
public class Caretaker {
	// 容纳备忘录的容器
	private HashMap<String, IMemento> memMap = new HashMap<String, IMemento>();

	public IMemento getMemento(String idx) {
		return memMap.get(idx);
	}

	public void setMemento(String idx, IMemento memento) {
		this.memMap.put(idx, memento);
	}
}
